# EPAM Systems coding task

#### Prerequisites:
* Java 11.
* Maven to build the project.
* Git client to obtain the sample code.
* Docker and docker-compose to start the application.
* Postman or curl in order to test the application.

#### Steps:

1. [What is supposed to do](#1-what-is-suppose-to-do)
2. [Assumptions](#2-assumptions)
3. [Cloning and building the project](#3-get-and-build-the-sample-code)
4. [Starting the application](#4-starting-the-application)
5. [Testing the application ](#5-test-the-application)

#### 1. What is the application supposed to do:

  #####Task: 
  - You need to implement an asynchronous application that registers clients and
  allows to modify their details. 
  - Your application must receive client details through a REST endpoint and then process the registration asynchronous.
  - Clients details must be stored in a database.
  - The application has to be built to scale horizontally.
  - The application must be thread safe and ensure data consistency.
  - Your application has to be containerized using Docker
  - You must provide instructions on how to run your application locally in a
  README.md
  
  #####Notes:
  -[x] We must be able to run your application locally based on your
  instructions
  - [x] Please show the best of your design and coding skills!
  - [x] Do not forget about tests
  - [x] The more effectively code is written – the better.

#### 2. Assumptions:
   
   My assumption were the following,
   
   A client is a object that has the following attributes: 
     
     {
       "firstName": "Andrei Gabriel",
       "lastName": "Muresan",
       "dateOfBirth": "1991-09-07",
       "emailAddress": "m.andreigabi91@gmail.com"
     }
    
   - For asynchronous registration part I've used an observer that listens for a specific
    event by using ApplicationEventPublisher which was configured as in 
    AsynchronousSpringEventsConfig.class.
   
   - For storing client data I've decided to use PostgreSQL.

#### 3. Cloning and building the project:
First, the repository must be cloned using the following commands:
    
    git clone https://gitlab.com/roandrei91/epamTask.git   
    cd to-cloned-directory
   
    
Then, build the code with Gradle. This will also run tests in order to make sure that everything is functioning properly. 
The repository includes the Gradle Wrapper `gradlew` which can be used if you don't currently have Gradle installed on your system.
    
    $ mvn clean install
    
    BUILD SUCCESSFUL in 1s
    2 actionable tasks: 2 executed
    
    
#### 4. Starting the application:
In order to run the application you need to execute the following commands:
    
    docker-compose up --build
To stop:
     
    docker-compose down
 
    
#### 5. Testing the application:
   ClientAdapter service is responsible for saving "Clients" and updating them. 
   Go to http://localhost:port/api/swagger-ui/, there you could see how endpoint are 
   documented. 
 
  