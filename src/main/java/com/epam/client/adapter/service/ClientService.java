package com.epam.client.adapter.service;

import com.epam.client.adapter.api.web.dto.ClientDto;
import com.epam.client.adapter.repository.postgres.ClientRepository;
import com.epam.client.adapter.repository.postgres.entity.Client;
import com.epam.client.adapter.service.converter.ClientDtoConverter;
import com.epam.client.adapter.service.exception.ClientAdapterException;
import com.epam.client.adapter.service.exception.ErrorCode;
import java.util.Optional;
import javax.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequiredArgsConstructor
@Slf4j
public class ClientService {

  private final ClientDtoConverter clientDtoConverter;
  private final ClientRepository clientRepository;

  @Transactional
  public void saveClient(ClientDto clientDto) {
    final Optional<Client> client = findClientIfExists(clientDto);
    if (client.isPresent()) {
      throw new ClientAdapterException(ErrorCode.CLIENT_ALREADY_EXISTS);
    }
    clientRepository.save(clientDtoConverter.createFrom(clientDto));
  }

  @Transactional
  public void updateClient(ClientDto clientDto) {
    var client = findClientIfExists(clientDto).orElseThrow(() -> new ClientAdapterException(ErrorCode.CLIENT_NOT_FOUND));
    clientRepository.save(client);
  }

  public Optional<Client> findClientIfExists(ClientDto clientDto) {
    return clientRepository.findByFirstNameAndLastName(clientDto.getFirstName(), clientDto.getLastName());
  }

}
