package com.epam.client.adapter.service.listener;

import com.epam.client.adapter.api.web.dto.ClientDto;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

@Getter
public class OnRegistrationEvent extends ApplicationEvent {

  private ClientDto clientDto;

  public OnRegistrationEvent(ClientDto clientDto) {
    super(clientDto);
    this.clientDto = clientDto;
  }

}
