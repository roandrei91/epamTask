package com.epam.client.adapter.service.exception;

public class ClientAdapterException extends RuntimeException {

  private final ErrorCode errorCode;

  public ClientAdapterException(ErrorCode errorCode){
    super(errorCode != null ? errorCode.getErrorName() : "");
    this.errorCode = errorCode;
  }

  public ErrorCode getErrorCode() {
    return errorCode;
  }

}
