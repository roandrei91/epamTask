package com.epam.client.adapter.service.listener;

import com.epam.client.adapter.service.ClientService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;

@RequiredArgsConstructor
@Slf4j
public class RegistrationListener implements ApplicationListener<OnRegistrationEvent> {

  private final ClientService clientService;

  @Override
  public void onApplicationEvent(OnRegistrationEvent onRegistrationEvent) {
    final var client = onRegistrationEvent.getClientDto();
    clientService.saveClient(client);
    log.info("Client successfully added to Postgres.");
  }

}
