package com.epam.client.adapter.service.converter;

import com.epam.client.adapter.api.web.dto.ClientDto;
import com.epam.client.adapter.repository.postgres.entity.Client;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class ClientDtoConverter implements DataConverter<ClientDto, Client> {

  @Override
  public Client createFrom(ClientDto object) {
    var client = new Client();
    client.setFirstName(object.getFirstName());
    client.setLastName(object.getLastName());
    client.setDateOfBirth(fromString(object.getDateOfBirth()));
    client.setEmailAddress(object.getEmailAddress());
    return client;
  }

  private ZonedDateTime fromString(String date) {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    LocalDate localDate = LocalDate.parse(date, formatter);
    return localDate.atStartOfDay(ZoneId.systemDefault());
  }

}
