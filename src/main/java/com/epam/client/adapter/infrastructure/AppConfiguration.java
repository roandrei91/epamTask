package com.epam.client.adapter.infrastructure;

import com.epam.client.adapter.api.web.validator.DateValidator;
import com.epam.client.adapter.api.web.validator.EmailValidator;
import com.epam.client.adapter.service.converter.ClientDtoConverter;
import com.epam.client.adapter.service.ClientService;
import com.epam.client.adapter.service.listener.RegistrationListener;
import com.epam.client.adapter.repository.postgres.ClientRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({AsynchronousSpringEventsConfig.class})
public class AppConfiguration {

  @Bean
  public ClientDtoConverter clientDtoConverter() {
    return new ClientDtoConverter();
  }

  @Bean
  public ClientService clientService(ClientDtoConverter clientDtoConverter,
       ClientRepository clientRepository) {
    return new ClientService(clientDtoConverter, clientRepository);
  }

  @Bean
  public RegistrationListener registrationListener(ClientService clientService) {
    return new RegistrationListener(clientService);
  }

  @Bean
  public EmailValidator emailValidator() {
    return new EmailValidator();
  }

  @Bean
  public DateValidator dateValidator() {
    return new DateValidator();
  }

}
