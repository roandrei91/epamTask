package com.epam.client.adapter.api.web.validator;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class DateValidator implements ConstraintValidator<ValidDate, String> {

  public static final String YYYY_MM_DD = "yyyy-mm-dd";

  @Override
  public void initialize(ValidDate constraintAnnotation) {

  }

  @Override
  public boolean isValid(String date, ConstraintValidatorContext constraintValidatorContext) {
    return checkDateFormat(date);
  }

  public boolean checkDateFormat(String dateStr) {
    DateFormat sdf = new SimpleDateFormat(YYYY_MM_DD);
    sdf.setLenient(false);
    try {
      sdf.parse(dateStr);
    } catch (ParseException e) {
      return false;
    }
    return true;
  }

}
