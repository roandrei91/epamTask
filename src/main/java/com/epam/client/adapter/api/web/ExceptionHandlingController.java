package com.epam.client.adapter.api.web;

import com.epam.client.adapter.api.web.dto.ErrorDto;
import com.epam.client.adapter.service.exception.ClientAdapterException;
import com.epam.client.adapter.service.exception.ErrorCode;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionHandlingController {

  @ExceptionHandler({ClientAdapterException.class})
  @ResponseBody
  public ResponseEntity<ErrorDto> handleValidatorException(ClientAdapterException e) {
    if (ErrorCode.CLIENT_ALREADY_EXISTS.name().equals(e.getErrorCode().name())) {
      return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(new ErrorDto(e.getErrorCode().getErrorName()));
    }
    if (ErrorCode.CLIENT_NOT_FOUND.name().equals(e.getErrorCode().name())) {
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorDto(e.getErrorCode().getErrorName()));
    }
    if (ErrorCode.INVALID_DATE_TIME.name().equals(e.getErrorCode().name())) {
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ErrorDto(e.getErrorCode().getErrorDetails()));
    }
    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorDto("Internal server error."));
  }

  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ResponseEntity<Map<String, Map<String, String>>> handleValidationExceptions(
      MethodArgumentNotValidException e) {
    Map<String, String> errorMap = e.getBindingResult()
        .getFieldErrors()
        .stream()
        .filter(o -> o.getDefaultMessage() != null)
        .collect(Collectors.toMap(FieldError::getField, DefaultMessageSourceResolvable::getDefaultMessage));

    return new ResponseEntity<>(Map.of("Input parameters validation errors", errorMap), HttpStatus.BAD_REQUEST);
  }
}
