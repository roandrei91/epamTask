package com.epam.client.adapter.api.web;

import com.epam.client.adapter.api.web.dto.ClientDto;
import com.epam.client.adapter.service.ClientService;
import com.epam.client.adapter.service.listener.OnRegistrationEvent;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/client")
@Validated
@Api(tags = "Client based operations.")
public class ClientController {

  private final ClientService clientService;
  private final ApplicationEventPublisher applicationEventPublisher;

  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(value = "Register new client.")
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Successfully register a client."),
      @ApiResponse(code = 400, message = "Input validation error."),
      @ApiResponse(code = 406, message = "Client already exists.")
  })
  public ResponseEntity createClient(@RequestBody @Valid ClientDto clientDto) {
    var onRegistrationEvent = new OnRegistrationEvent(clientDto);
    log.info("Put OnRegistrationEvent to applicationEventPublisher.");
    applicationEventPublisher.publishEvent(onRegistrationEvent);
    return ResponseEntity.ok().build();
  }

  @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(value = "Update an existing client.")
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Successfully updated a client."),
      @ApiResponse(code = 400, message = "Input validation error."),
      @ApiResponse(code = 404, message = "No Client found.")
  })
  public ResponseEntity updateClient(@RequestBody @Valid ClientDto clientDto) {
    clientService.updateClient(clientDto);
    return ResponseEntity.ok().build();
  }

}
