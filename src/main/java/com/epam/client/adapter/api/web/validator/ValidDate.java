package com.epam.client.adapter.api.web.validator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Constraint(validatedBy = DateValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidDate {

  String message() default "Invalid date. Date should be in format yyyy-MM-dd. Example: 2020-12-20";

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};

}
