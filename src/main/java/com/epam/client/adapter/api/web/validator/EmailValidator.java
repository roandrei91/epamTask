package com.epam.client.adapter.api.web.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class EmailValidator implements ConstraintValidator<ValidEmail, String> {

  private static final String EMAIL_REGEX = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
  private static final Pattern EMAIL_PATTERN = Pattern
      .compile(EMAIL_REGEX, Pattern.CASE_INSENSITIVE);

  @Override
  public void initialize(ValidEmail constraintAnnotation) {

  }

  @Override
  public boolean isValid(String emailAddress, ConstraintValidatorContext constraintValidatorContext) {
    return validateEmailAddressAccordingToRFC5322(emailAddress);
  }

  public boolean validateEmailAddressAccordingToRFC5322(String address) {
    Matcher matcher = EMAIL_PATTERN.matcher(address);
    return matcher.find();
  }
}
