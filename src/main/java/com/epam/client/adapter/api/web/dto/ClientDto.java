package com.epam.client.adapter.api.web.dto;

import com.epam.client.adapter.api.web.validator.ValidDate;
import com.epam.client.adapter.api.web.validator.ValidEmail;
import javax.validation.constraints.NotBlank;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ClientDto {

  @NotBlank
  private String firstName;

  @NotBlank
  private String lastName;

  @ValidDate
  private String dateOfBirth;

  @ValidEmail
  private String emailAddress;

}
