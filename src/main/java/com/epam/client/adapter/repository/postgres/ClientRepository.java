package com.epam.client.adapter.repository.postgres;

import com.epam.client.adapter.repository.postgres.entity.Client;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends CrudRepository<Client, Long> {

  Optional<Client> findByFirstNameAndLastName(final String firstName, final String lastName);

}
