package com.epam.client.adapter.repository.postgres.entity;


import java.time.ZonedDateTime;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.Data;

@Data
@Entity
public class Client {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  private String firstName;
  private String lastName;
  private ZonedDateTime dateOfBirth;
  private String emailAddress;

  public static Client createInstance(final String firstName, final String lastName, final
      ZonedDateTime dateOfBirth, final String emailAddress) {
    final var client = new Client();
    client.setFirstName(firstName);
    client.setLastName(lastName);
    client.setDateOfBirth(dateOfBirth);
    client.setEmailAddress(emailAddress);
    return client;
  }

}
