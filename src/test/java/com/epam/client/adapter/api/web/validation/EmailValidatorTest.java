package com.epam.client.adapter.api.web.validation;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.epam.client.adapter.api.web.validator.EmailValidator;
import org.junit.jupiter.api.Test;

public class EmailValidatorTest {

  @Test
  void testValidateEmailAddressAccordingToRFC5322() {
    var emailValidator = new EmailValidator();

    final String email = "test@epam.com";
    assertTrue(emailValidator.validateEmailAddressAccordingToRFC5322(email));

    final String wrongEmailFormat = "@c.com";
    assertFalse(emailValidator.validateEmailAddressAccordingToRFC5322(wrongEmailFormat));

  }
}
