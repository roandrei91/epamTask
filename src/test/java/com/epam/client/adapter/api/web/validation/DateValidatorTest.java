package com.epam.client.adapter.api.web.validation;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.epam.client.adapter.api.web.validator.DateValidator;
import org.junit.jupiter.api.Test;

public class DateValidatorTest {

  @Test
  void testCheckDateFormat() {
    final DateValidator dateValidator = new DateValidator();

    //date format should be yyyy-mm-dd
    String date = "2000-10-10";
    assertTrue(dateValidator.checkDateFormat(date));

    date = "10-10-2000";
    assertFalse(dateValidator.checkDateFormat(date));
  }
}
