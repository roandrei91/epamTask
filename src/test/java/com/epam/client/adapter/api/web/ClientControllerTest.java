package com.epam.client.adapter.api.web;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.epam.client.adapter.api.web.dto.ClientDto;
import com.epam.client.adapter.repository.postgres.entity.Client;
import com.epam.client.adapter.service.ClientService;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = ClientController.class)
public class ClientControllerTest {

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private ClientService clientService;

  @Autowired
  private ObjectMapper objectMapper;

  @Test
  void testInputIsInvalid_onCreateClient_thenReturnStatus400() throws Exception {
    var invalidClientDto = invalidClientDto();
    final String body = objectMapper.writeValueAsString(invalidClientDto);

    willDoNothing().given(clientService).saveClient(invalidClientDto);

    mockMvc.perform(post("/client")
        .contentType(MediaType.APPLICATION_JSON)
        .content(body))
        .andExpect(status().isBadRequest());

  }

  @Test
  void testInputIsInvalid_onUpdateClient_thenReturnStatus400() throws Exception {
    var invalidClientDto = secondInvalidClientDto();
    final String body = objectMapper.writeValueAsString(invalidClientDto);

    willDoNothing().given(clientService).saveClient(invalidClientDto);

    mockMvc.perform(put("/client")
        .contentType(MediaType.APPLICATION_JSON)
        .content(body))
        .andExpect(status().isBadRequest());
  }

  @Test
  void testCreateClient_happyFlow() throws Exception {
    var clientDto = clientDto();
    final String body = objectMapper.writeValueAsString(clientDto);

    given(clientService.findClientIfExists(clientDto)).willReturn(
        (Optional.of(client())));

    mockMvc.perform(post("/client")
       .contentType(MediaType.APPLICATION_JSON)
       .content(body))
        .andExpect(status().isOk());
  }

  @Test
  void testUpdateClient_happyFlow() throws Exception {
    var clientDto = clientDto();
    final String body = objectMapper.writeValueAsString(clientDto);

    given(clientService.findClientIfExists(clientDto)).willReturn(
        (Optional.of(client())));

    mockMvc.perform(put("/client")
        .contentType(MediaType.APPLICATION_JSON)
        .content(body))
        .andExpect(status().isOk());
  }

  private ClientDto invalidClientDto() {
    return ClientDto.builder().firstName("FirstName").lastName("LastName").dateOfBirth("2000-10-10")
        .emailAddress("@test.com").build();
  }

  private ClientDto secondInvalidClientDto() {
    return ClientDto.builder().firstName("FirstName").lastName("LastName").dateOfBirth("10-10-2000")
        .emailAddress("firstName@test.com").build();
  }

  private ClientDto clientDto() {
    return ClientDto.builder().firstName("FirstName").lastName("LastName").dateOfBirth("1991-09-07")
        .emailAddress("firstName@test.com").build();
  }

  private Client client() {
    var client = Client
        .createInstance("FirstName", "LastName",
            fromString("1991-09-07"), "test@epam.com");
    return client;
  }

  private ZonedDateTime fromString(String date) {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    LocalDate localDate = LocalDate.parse(date, formatter);
    return localDate.atStartOfDay(ZoneId.systemDefault());
  }

}
